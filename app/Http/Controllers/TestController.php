<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ProxmoxVE\Proxmox;

class TestController extends Controller
{
    public function test(Request $request){
// Create your credentials array
        $credentials = [
            'hostname' => '192.168.116.129',
            'username' => 'root',
            'password' => '123456',
            'realm' => 'pam',
            'port' => '8006',
            ];

// Pass your credentialpps when creating the Proxmox API Client object.
        $proxmox = new Proxmox($credentials);


        // Prepare params to use
        $newLXC = [
            'action' => $request->action,
            'type' => $request->type,

        ];

// Create a new proxmox user
        $result = $proxmox->create('/nodes/pve/firewall/rules', $newLXC);
return redirect()->back();
    }

    public function  index(){
        $credentials = [
            'hostname' => '192.168.116.129',
            'username' => 'root',
            'password' => '123456',
            'realm' => 'pam',
            'port' => '8006',
        ];

        $proxmox = new Proxmox($credentials);
$response= $proxmox->get('/nodes/pve/firewall/rules/');
        $rules= $response['data'];

        return view('add',compact('rules'));
    }


    public function  deleterule($pos){

        $credentials = [
            'hostname' => '192.168.116.129',
            'username' => 'root',
            'password' => '123456',
            'realm' => 'pam',
            'port' => '8006',
        ];

// Pass your credentialpps when creating the Proxmox API Client object.
        $proxmox = new Proxmox($credentials);
        $proxmox->delete("/nodes/pve/firewall/rules/$pos");
        return redirect()->back();





    }
}
