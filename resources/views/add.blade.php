<!DOCTYPE html>
<html>
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<h1>ROLES</h1>

<form action="/addrole" method="post">
    @csrf
    <br>
    <div class="form-group">

        <label class="sr-only" for="email">ACTION:</label>

        <select class="form-control" name="action" >
        <option value="ACCEPT">ACCEPT</option>
        <option value="DROP">DROP</option>
        <option value="REJECT">REJECT</option>
    </select>
    </div>
    <div class="form-group">
        <label class="sr-only" for="email">TYPE:</label>

    <select class="form-control" name="type" >
        <option value="in">in</option>
        <option value="out">out</option>
        <option value="group">group</option>
    </select>
    </div>
    <br><br>
    <input type="submit" value="Submit">
</form>
<br><hr>

<table class="table">
    <tr>
        <th>pos</th>
        <th>action</th>
        <th>type</th>
        <th>delete</th>
    </tr>
    @foreach($rules as $rule)
    <tr>
        <td>{{$rule['pos']}}</td>
        <td>{{$rule['action']}}</td>
        <td>{{$rule['type']}}</td>
        <td><a href="/deleterule/{{$rule['pos']}}">Delete</a></td>
    </tr>
   @endforeach
</table>
</div>

</body>
</html>
